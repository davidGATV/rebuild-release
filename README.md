# Recommender-system-REBUILD

A Hybrid Recommender System based on the user-profile

## Set up the system

### Environment variables 

| Variable | Description | Needed udpdate
| ------ | ------ | ------ |
| API_HOST | API Host| No |
| API_PORT | API Port| No |
| MONGODB_HOST | Mongo Database Host| Yes |
| MONGODB_PORT | Mongo Database Port| Yes |
| MONGODB_USERNAME | Mongo Database Username| Yes |
| MONGODB_PASSWORD | Mongo Database Password| Yes |
| MONGODB_NAME | Mongo Database Name | Yes |
| USER_COLLECTION | Mongo Database Collection for Users | Yes |
| JOB_COLLECTION | Mongo Database Collection for Jobs | Yes |
| SKILL_COLLECTION | Mongo Database Collection for Skills | Yes |
| PLACE_COLLECTION | Mongo Database Collection for Places | Yes |
| CONTENT_COLLECTION | Mongo Database Collection for Digital Content | Yes |
| EVENT_COLLECTION | Mongo Database Collection for Events | Yes |
| USER_EMBEDDING_COLLECTION | Mongo Database Collection for User embeddings | Yes |
| ITEM_EMBEDDING_COLLECTION | Mongo Database Collection for Item embeddings | Yes |
