from pymongo import MongoClient
from pymongo.database import Database
from mongoengine import connect
from bson.objectid import ObjectId
from helper import global_variables as gv
from typing import Optional


class MongoDBManager:
    def __init__(self, host: str, port: str, username: str = "", password: str = ""):

        self.db_port: int = int(port)
        self.db_host: str = host
        self.username: str = username
        self.password: str = password

        self.db_url = ("mongodb://" + self.username + ":"+ self.password + "@" +
                       self.db_host + ":" + str(self.db_port) + "/")

        self.db_name: str = gv.db_name
        self.db_client: Optional[MongoClient] = None
        self.db:  Optional[Database] = None
        self.status = 200

    def establish_client_connection(self):
        """[summary]
        """
        try:
            gv.logger.info("Connection established with MongoDB at {}:{}".format(
                self.db_host, self.db_port
            ))
            self.db_client: MongoClient = MongoClient(self.db_host, self.db_port)
        except Exception as e:
            gv.logger.error(e)

    def create_db_connection(self):
        try:
            if self.db_client is not None:
                # If no exists
                if not self.check_database_exists():
                    gv.logger.info("A new db was created!")

                self.db: Database = self.db_client[self.db_name]
                connect(self.db_name, host=self.db_host, port=int(self.db_port))
                self.status = 200
            else:
                gv.logger.warning("Create a client connection first!")
                self.status = 405
        except Exception as e:
            gv.logger.error(e)
            self.status = 404

    def set_up_db(self):
        try:
            # Establish connection
            self.establish_client_connection()
            # Create new instance of the db
            self.db = self.db_client[self.db_name]
        except Exception as e:
            gv.logger.error(e)

    def check_dbManager(self):
        """Checks if dbManager has started. If not, initialize dbManager.
        """
        try:
            if self.db_client is None:
                # If no exists
                if not self.check_database_exists():
                    gv.logger.info("A new DB has been created")
                self.establish_client_connection()
                self.db = self.db_client[self.db_name]
        except Exception as e:
            gv.logger.error(e)
            self.status = 500

    def create_new_collection_instance(self, collection_name):
        try:
            # If no exists
            if self.db is not None:
                # Check if the collection already exists
                collection = self.db[collection_name]
                self.status = 200
            else:
                gv.logger.warning("The connection to the db is not established!")
                self.status = 405
        except Exception as e:
            gv.logger.error(e)
            self.status = 404

    def check_database_exists(self):
        """[summary]

        :return: [description]
        :rtype: [type]
        """
        try:
            response = True
            dblist = self.db_client.list_database_names()
            if self.db_name not in dblist:
                response = False
                self.status = 200
        except Exception as e:
            gv.logger.error(e)
            self.status = 404
            response = None
        return response

    def insert_document_to_collection(self, collection_name: str, document: dict):
        response: bool = False
        try:
            self.check_dbManager()
            if self.db is not None:
                self.db[collection_name].insert_one(document)
                response: bool = True
            else:
                gv.logger.warning("Unable to insert data into collection %s", collection_name)
        except Exception as e:
            gv.logger.error(e)
            self.status = 404
        return response

    def close_connection(self):
        try:
            self.db_client.close()
        except Exception as e:
            gv.logger.error(e)

    def get_all_documents_from_collection(self, collection_name: str):
        documents: list = []
        try:
            if self.db is not None:
                documents: list = list(self.db[collection_name].find({}))
        except Exception as e:
            gv.logger.error(e)
        return documents

    def get_document_by_id(self, collection_name: str, uuid: str):
        document: dict = {}
        try:
            document = self.db[collection_name].find_one({"_id": ObjectId(uuid)})
        except Exception as e:
            gv.logger.error(e)
        return document