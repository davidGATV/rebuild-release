from py2neo import Graph
import json
import pandas as pd
from helper import global_variables as gv


class Neo4j:
    def __init__(self, protocol: str, host: str, port: str,
                 username: str, password: str):
        self.protocol = protocol
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.uri = self.protocol + "://" + self.host + ':' + self.port + "/db/data/"
        self.graph = None

    def init_graph(self):
        try:
            self.graph = Graph(uri=self.uri, auth=(self.username,
                                                   self.password))
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def execute_constraint_query(graph: Graph, labels: list):
        try:
            # Constraint query
            if len(graph.schema.node_labels) == 0 and len(graph.schema.relationship_types) == 0:
                for label in labels:
                    uuid = "uuid"
                    query = __class__.constraint_query(label=label, uuid=uuid)
                    __class__.run_query(graph=graph, query=query)
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def constraint_query(label: str, uuid: str):
        query = None
        try:
            query = """CREATE CONSTRAINT ON (i:LABEL) ASSERT (i.UUID) 
            IS UNIQUE""".replace("LABEL", label).replace("UUID", uuid)
        except Exception as e:
            gv.logger.error(e)
        return query

    @staticmethod
    def create_node(label: str, params_keys: list):
        query = None
        try:
            art_values = ["$" + k for k in params_keys]
            params = json.dumps(dict(zip(params_keys, art_values))).replace(": ", ":").replace('\"', "")
            query = """MERGE (a:LABEL PARAMS)""".replace("PARAMS", params).replace("LABEL",
                                                                                   label)
        except Exception as e:
            gv.logger.error(e)
        return query

    @staticmethod
    def run_query(graph: Graph, query: str, data=False):
        response = {"status": 200, "error": ""}
        try:
            if data:
                response["response"] = graph.run(query).data()
            else:
                response["response"] = graph.run(query)
        except Exception as e:
            gv.logger.error(e)
            response = {"status": 400, "error": str(e), "data": []}
        return response

    @staticmethod
    def create_node_from_df(graph: Graph, query: str, df: pd.DataFrame):
        response = {"status": 200, "error": ""}
        try:
            # Start graph
            tx = graph.begin()
            for index, row in df.iterrows():
                try:
                    parameters = dict(row)
                    tx.evaluate(query, parameters=parameters)
                except Exception as e:
                    gv.logger.warning(e)
                    continue
            # Commit the process
            tx.commit()
        except Exception as e:
            gv.logger.warning(e)
            response = {"status": 400, "error": str(e)}
        return response

    @staticmethod
    def remove_database(graph: Graph):
        response = {"status": 200, "error": ""}
        try:
            query = """MATCH (n) DETACH DELETE n"""
            graph.run(query)
        except Exception as e:
            gv.logger.error(e)
            response = {"status": 400, "error": str(e)}
        return response

    @staticmethod
    def create_relationship(label_a: str, label_b: str, relationship: str, unwind: str, uuid: str):
        query = None
        try:
            query = """MATCH(i:LABEL_A) UNWIND i.UNWIND_PARAM as param MATCH(a:LABEL_B)
             WHERE param = a.UUID MERGE (i)-[:RELATIONSHIP]-(a)""".replace(
                "LABEL_A", label_a).replace("LABEL_B", label_b).replace("UNWIND_PARAM",
                                                                        unwind).replace("UUID",
                                                                                        uuid).replace(
                "RELATIONSHIP", relationship)
        except Exception as e:
            gv.logger.error(e)
        return query

    @staticmethod
    def reverse_relationship(label_a: str, label_b: str, relationship: str, unwind: str, uuid: str):
        query = None
        try:
            query = """MATCH(i:LABEL_A) UNWIND i.UNWIND_PARAM as param MATCH(a:LABEL_B)
                     WHERE param = a.UUID MERGE (a)-[:RELATIONSHIP]-(i)""".replace(
                "LABEL_A", label_a).replace("LABEL_B", label_b).replace("UNWIND_PARAM",
                                                                        unwind).replace("UUID",
                                                                                        uuid).replace(
                "RELATIONSHIP", relationship)
        except Exception as e:
            gv.logger.error(e)
        return query

    @staticmethod
    def create_undirect_relationship(label_a: str, label_b: str, label_c: str, relationship: str,
                                     unwind_b: str, unwind_c: str, uuid: str):
        query = None
        try:
            query_doc = """MATCH (i:LABEL_A) WITH i UNWIND i.UNWIND_PARAMS_B as param_b UNWIND
             i.UNWIND_PARAMS_C as param_c MATCH(o:LABEL_B) MATCH(a:LABEL_C) WHERE param_c
              = a.UUID AND param_b =o.UUID MERGE (a)-[:RELATIONSHIP]-(o)"""
            query = query_doc.replace("LABEL_A", label_a).replace(
                "LABEL_B", label_b).replace("LABEL_C", label_c).replace(
                "RELATIONSHIP", relationship).replace(
                "UNWIND_PARAMS_B", unwind_b).replace("UNWIND_PARAMS_C", unwind_c).replace("UUID", uuid)
        except Exception as e:
            gv.logger.error(e)
        return query

    @staticmethod
    def set_weights_to_relationship(label_a: str, label_b: str, relationship: str,
                                     unwind: str, uuid: str, weight_property: str,
                                     weight_label: str):
        query = None
        try:
            query = """MATCH(i:LABEL_A) UNWIND i.UNWIND_PARAM as param 
            MATCH(a:LABEL_B) UNWIND i.UNWIND_WEIGHT as weight 
                 WHERE param = a.UUID MERGE (i)-[:RELATIONSHIP WP: WL]-(a)""".replace(
                "LABEL_A", label_a).replace("LABEL_B", label_b).replace("UNWIND_PARAM",
                                                                        unwind).replace("UUID",
                                                                                        uuid).replace(
                "RELATIONSHIP", relationship).replace("WP", weight_property).replace("UNWIND_WEIGHT", weight_label)
        except Exception as e:
            gv.logger.error(e)
        return query

    @staticmethod
    def retrieve_connected_nodes(label_a: str, label_b: str, uuid: str, query_uuid: str,
                                 response: str, query_data: str, response_data: str):
        query = None
        try:
            query = """MATCH (u:LABEL_A) WHERE u.PARAM_UUID = QUERY_UUID
            WITH u MATCH (u)-[*]->(j:LABEL_B) RETURN j.PARAM_UUID as RESPONSE_QUERY, j.PARAM_DATA as RESPONSE_DATA
            """

            query = query.replace("LABEL_A", label_a).replace(
                "LABEL_B", label_b).replace(
                "PARAM_UUID", uuid).replace(
                "QUERY_UUID", '"' + query_uuid + '"').replace(
                "RESPONSE_QUERY", response).replace(
                "PARAM_DATA", query_data).replace(
                "RESPONSE_DATA", response_data)
        except Exception as e:
            gv.logger.error(e)
        return query

    @staticmethod
    def retrieve_node_properties(label: str, property_name: str, value: str, response: str):
        query = None
        try:
            query = """MATCH (u: LABEL) WHERE u.PROPERTY = VALUE RETURN u as RESPONSE"""
            query = query.replace("LABEL", label).replace(
                "PROPERTY", property_name).replace("VALUE", '"' + value + '"').replace("RESPONSE", response)
        except Exception as e:
            gv.logger.error(e)
        return query