import os
from helper.custom_log import init_logger
import warnings
warnings.filterwarnings("ignore")

# APP
host = os.getenv("API_HOST") if "API_HOST" in os.environ else "localhost"
port = int(os.getenv("API_PORT")) if "API_PORT" in os.environ else 5000


# Neo4j
neo4j_protocol = "bolt"
neo4j_host = "localhost"
neo4j_port = "7687"
neo4j_username = "developer"
neo4j_password = "developer"

# Elasticsearch
es_host = "localhost"
es_port = "9200"

# ==============================
db_host = os.getenv("MONGODB_HOST") if "MONGODB_HOST" in os.environ else "localhost"
db_port = os.getenv("MONGODB_PORT") if "MONGODB_PORT" in os.environ else "27017"
db_username = os.getenv("MONGODB_USERNAME") if "MONGODB_USERNAME" in os.environ else ""
db_password = os.getenv("MONGODB_PASSWORD") if "MONGODB_PASSWORD" in os.environ else ""
db_name = os.getenv("MONGODB_NAME") if "MONGODB_NAME" in os.environ else "REBUILD"

users_index_name = os.getenv("USER_COLLECTION") if "USER_COLLECTION" in os.environ else "users_rebuild"
jobs_index_name = os.getenv("JOB_COLLECTION") if "JOB_COLLECTION" in os.environ else "jobs_rebuild"
skills_index_name = os.getenv("SKILL_COLLECTION") if "SKILL_COLLECTION" in os.environ else "skills_rebuild"
places_index_name = os.getenv("PLACE_COLLECTION") if "PLACE_COLLECTION" in os.environ else "places_rebuild"
content_index_name = os.getenv("CONTENT_COLLECTION") if "CONTENT_COLLECTION" in os.environ else "contents_rebuild"
event_index_name = os.getenv("EVENT_COLLECTION") if "EVENT_COLLECTION" in os.environ else "events_rebuild"
user_embd_index_name = os.getenv("USER_EMBEDDING_COLLECTION") if "USER_EMBEDDING_COLLECTION" in os.environ else \
    "user_embeddings"
item_embd_index_name = os.getenv("ITEM_EMBEDDING_COLLECTION") if "ITEM_EMBEDDING_COLLECTION" in os.environ else \
    "item_embeddings"
all_embeddings = False

# Parameters
logger = None
batch_threads = []
# recommender_api: RecommenderAPI = RecommenderAPI()


def init():
    global logger
    logger = init_logger(__name__, testing_mode=False)
