import hashlib
import requests
import universities
from datetime import date, timedelta, datetime
from faker import Faker
from models.object_dict import ObjectDict
from random import randrange
import numpy as np
from helper import global_variables as gv


def generate_uuid_from_string(data_uuid: list):
    uuid: str = ""
    try:
        # Concatenate elements from list
        uuid_data: str = "".join(data_uuid)
        hash_object = hashlib.sha512(uuid_data.encode('utf-8'))
        uuid: str = hash_object.hexdigest()
    except Exception as e:
        gv.logger.error(e)
    return uuid

def get_location(faker: {Faker}):
    city, country_code = "", ""
    try:
        print(faker.locales)
        code = faker.locales[0].split("_")[-1]
        loc = faker.local_latlng(code, coords_only=False)

        city = loc[2]
        country_code = loc[3]
    except Exception as e:
        gv.logger.error(e)
    return city, country_code


def get_country_info(country_code: {str}):
    response = {}
    try:
        url = "https://restcountries.eu/rest/v2/alpha/" + country_code
        response = ObjectDict(requests.get(url=url).json())
        while "languages" not in response:
            print("---- Response without languages ----")
            response = ObjectDict(requests.get(url=url).json())

    except Exception as e:
        gv.logger.error(e)
    return response


def get_n_jobs(n_jobs=1500, limit=50):
    response = []
    try:
        url = "http://api.dataatwork.org/v1/jobs"
        headers = {"Accept": "application/json"}
        gv.logger.info("Retrieving Jobs ...")
        done = True
        offset = 10
        while done:
            params = {"offset": offset, "limit": limit}
            res = requests.get(url=url, params=params, headers=headers).json()
            response += [i for i in res if "uuid" in list(i.keys())]
            if len(response) >= n_jobs:
                done = False
            else:
                offset += 1000
    except Exception as e:
        gv.logger.error(e)
    return response


def get_skills_for_job(job_id):
    response = {}
    try:
        url = "http://api.dataatwork.org/v1/jobs/%s/related_skills" % (str(job_id))
        headers = {"Accept": "application/json"}
        gv.logger.info("Retrieving Skills for Job ID %s ...", str(job_id))
        response = requests.get(url=url, headers=headers).json()
    except Exception as e:
        gv.logger.error(e)
    return response


def calculate_age_from_birth_year(birth_year: {int}):
    age = -1
    try:
        age = int(datetime.now().year - birth_year)
    except Exception as e:
        gv.logger.error(e)
    return age


def random_date(start_date: {date}, end_date: {date}):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = randrange(days_between_dates)
    random_date = start_date + timedelta(days=random_number_of_days)
    return random_date


def generate_university_experience(country: {str}, age: {int}):
    response = None
    try:
        response = {"institute": "", "started_at": "", "ended_at": "",
                    "type": "university"}

        uni = universities.API()
        res_uni = list(uni.search(country=country))

        if res_uni:
            response["institute"] = np.random.choice(res_uni).name
        else:
            response["institute"] = "Not available"

        started_year = datetime.now().year - (age - 19)
        ended_year = datetime.now().year - (age - 25)

        response["started_at"] = date(started_year, 9, 1)
        response["ended_at"] = date(ended_year, 9, 1)

    except Exception as e:
        gv.logger.error(e)
    return response


def generate_school_experience(country: {str}, age: {int}, high_school=False):
    response = None
    try:
        response = {"institute": "", "started_at": "", "ended_at": "",
                    "type": "school"}

        uni = universities.API()
        res_uni = list(uni.search(country=country))

        if res_uni:
            institute = np.random.choice(res_uni).name
        else:
            institute = "Not available"

        response["institute"] = institute.lower().replace("university", "").title()

        if high_school:
            started_year = datetime.now().year - (age - 12)
            ended_year = datetime.now().year - (age - 17)
        else:
            started_year = datetime.now().year - (age - 6)
            ended_year = datetime.now().year - (age - 11)

        response["started_at"] = date(started_year, 9, 1)
        response["ended_at"] = date(ended_year, 9, 1)

    except Exception as e:
        gv.logger.error(e)
    return response


def generate_work_experience(age: {int}, profile:{dict}, ended_date: {date}, min_age=25):
    response = None
    try:

        started_year = datetime.now().year - (age - min_age)
        if ended_date is None:
            ended_year = datetime.now().year
            ended_date = date(ended_year, 9, 1)

        response = {"category": profile["job"], "company": profile["company"],
                    "job_task": "", "started_at": date(started_year, 9, 1),
                    "ended_at": ended_date}
    except Exception as e:
        gv.logger.error(e)
    return response