from helper import global_variables as gv
from keras.models import Model
from keras.layers import (Input, concatenate, Dense, Dropout)
from typing import Optional


class SkillMatching:
    def __init__(self):
        self.users: iter = ([])
        self.items: iter = ([])
        self.output_dim = 1
        self.name = "based-skill-matching"

    def set_users(self, users: iter):
        self.users: iter = users

    def set_items(self, items: iter):
        self.items = items

    def base_model(self, user_embedding_shape: tuple, item_embedding_shape: tuple):
        model: Optional[Model] = None
        try:
            # Input Layers
            input_user = Input(shape=user_embedding_shape)
            input_item = Input(shape=item_embedding_shape)

            # Concatenate encodings
            x = concatenate([input_user, input_item])

            # Hidden Layers
            z = Dense(1024, activation="tanh", name="concatenate")(x)
            z = Dropout(0.5)(z)

            z = Dense(512, activation="tanh", name="concatenate")(z)
            z = Dropout(0.5)(z)
            z = Dense(256, activation="tanh", name="concatenate")(z)
            z = Dropout(0.5)(z)

            # Output Layer
            y = Dense(self.output_dim, activation="sigmoid")(z)

            model: Model = Model(inputs=[input_user, input_item],
                                 outputs=y,
                                 name=self.name)
        except Exception as e:
            gv.logger.error(e)
        return model






