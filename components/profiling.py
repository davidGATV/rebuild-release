from helper import global_variables as gv
from flair.embeddings import (FlairEmbeddings, DocumentPoolEmbeddings, Sentence,
                              BertEmbeddings, WordEmbeddings, DocumentRNNEmbeddings)
from typing import Optional
from tqdm import tqdm


class Profile:
    def __init__(self):
        self.flair_embedding_forward: Optional[FlairEmbeddings] = None
        self.flair_embedding_backward: Optional[FlairEmbeddings] = None
        self.bert_embedding: Optional[FlairEmbeddings] = None
        self.glove_embedding: Optional[FlairEmbeddings] = None
        self.embeddings: list = []
        self.rnn_type: str = 'LSTM'
        self.hidden_size: int = 512
        self.response: iter = iter([])

    def initialize_document_embeddings(self):
        try:
            # initialise embedding classes
            self.flair_embedding_forward = FlairEmbeddings("news-forward")
            self.flair_embedding_backward = FlairEmbeddings("news-backward")
            self.bert_embedding = BertEmbeddings("bert-base-uncased")
            self.glove_embedding = WordEmbeddings('glove')
            self.embeddings = [self.flair_embedding_backward, self.flair_embedding_backward,
                               self.bert_embedding, self.glove_embedding]
        except Exception as e:
            gv.logger.error(e)

    def build_embedding(self, all_embd: bool = False):
        if all_embd:
            embeddings: list = self.embeddings
        else:
            embeddings: list = [self.bert_embedding, self.glove_embedding]
        return embeddings


class UserProfile(Profile):
    def __init__(self):
        Profile.__init__(self)
        self.initialize_document_embeddings()

    def perform_user_profiling(self, users: iter, use_rnn: bool = True,
                               all_embeddings: bool = False):
        try:
            output: list = []
            data_source, data_uuid = self.extract_data(users=users)
            embeddings: list = self.build_embedding(all_embd=all_embeddings)

            if not use_rnn:
                document_embeddings: DocumentPoolEmbeddings = DocumentPoolEmbeddings(embeddings)
            else:
                document_embeddings: DocumentPoolEmbeddings = DocumentRNNEmbeddings(embeddings,
                                                                                    rnn_type=self.rnn_type,
                                                                                    hidden_size=self.hidden_size)

            if data_source and data_uuid:
                sentence_data = self.get_sentences_data(data_source=data_source)
                sentences = [Sentence(i) for i in sentence_data]
                embeddings_data: list = []

                # Embedding sentences
                gv.logger.info("Extracting document embeddings for Users")
                with tqdm(total=len(sentences)) as pbar:
                    for i, s in enumerate(sentences):
                        document_embeddings.embed(s)
                        single_embd: list = s.get_embedding().detach().numpy().tolist()
                        embeddings_data.append(single_embd)
                        pbar.update(1)
                gv.logger.info("Done!")
                # Return into iterator
                output = [{'embedding': embd, 'uuid': uuid} for embd, uuid in zip(embeddings_data, data_uuid)]
            self.response = iter(output)
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def extract_data(users: iter):
        data_source: list = []
        data_uuid: list = []
        try:
            for user in users:
                data_uuid += [str(user.get('_id'))]
                data_source += [user["studies_level"] + user["mother_tongue"]]

                # data_source += [j["_source"] for j in user]
                # data_uuid += [j["_id"] for j in user]
        except Exception as e:
            gv.logger.error(e)
        return data_source, data_uuid

    @staticmethod
    def get_sentences_data(data_source: list):
        sentence_data: list = []
        try:
            # TODO: ADD ALL DATA NECESSARY FOR USER
            for i in data_source:
                sentence_data.append(i)
        except Exception as e:
            gv.logger.error(e)
        return sentence_data

    @staticmethod
    def parse_user_profile(user_data: {dict}):
        sent_data: str = ""
        try:
            pass
        except Exception as e:
            gv.logger.error(e)
        return sent_data


class ItemProfile(Profile):
    def __init__(self):
        Profile.__init__(self)
        self.initialize_document_embeddings()

    def perform_item_profiling(self, items: iter, item_key: str, use_rnn: bool = True,
                               all_embeddings: bool = False):
        try:
            data_uuid: list = []
            embeddings_data: list = []
            # Embedding sentences
            gv.logger.info("Extracting document embeddings for item of type %s", item_key)
            gv.logger.info("Done!")
            # Return into iterator
            output = [{'embedding': embd, 'uuid': uuid} for embd, uuid in zip(embeddings_data, data_uuid)]
            self.response = iter(output)
        except Exception as e:
            gv.logger.error(e)
        return self.response

    @staticmethod
    def extract_data(items: iter, item_key: str):
        data_source: list = []
        data_uuid: list = []
        try:
            for i in items:
                if item_key in i.keys():
                    data_info = i[item_key]
                    data_source += [j["_source"] for j in data_info]
                    data_uuid += [j["_id"] for j in data_info]
        except Exception as e:
            gv.logger.error(e)
        return data_source, data_uuid

    @staticmethod
    def get_sentences_data(data_source: list, item_key: str):
        sentence_data: list = []
        try:
            # TODO: ADD ALL CASES FOR THE DATA AVAILABLE IN THE PLATFORM
            for i in data_source:
                # JOB
                if item_key == "job":
                    sent_data = __class__.parse_job_data(data=i)
                # INTEREST
                else:
                    sent_data = i["name"]
                sentence_data.append(sent_data)
        except Exception as e:
            gv.logger.error(e)
        return sentence_data

    @staticmethod
    def parse_job_data(data: dict):
        sent_data: str = ""
        try:
            if len(data["description"]) == 0:
                data["description"] = "default description."
            sent_data = data["job_title"] + ". " + data["description"] +\
                        ", ".join(data["skill_names"])
        except Exception as e:
            gv.logger.error(e)
        return sent_data