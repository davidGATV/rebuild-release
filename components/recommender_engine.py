from helper import global_variables as gv
from managers.neo4j_connector import Neo4j
from typing import Optional
from components.profiling import UserProfile, ItemProfile
from py2neo import Graph


class HybridRecommenderEngine:
    def __init__(self, use_rnn_embedding: bool = False):
        self.data: Optional[iter] = None
        self.users: Optional[iter] = None
        self.items: Optional[iter] = None

        self.neo4j_protocol: str = gv.neo4j_protocol
        self.neo4j_host: str = gv.neo4j_host
        self.neo4j_port: str = gv.neo4j_port
        self.neo4j_username: str = gv.neo4j_username
        self.neo4j_password: str = gv.neo4j_password
        self.neo4j_connector: Neo4j = Neo4j(protocol=self.neo4j_protocol, host=self.neo4j_host,
                                            port=self.neo4j_port, username=self.neo4j_username,
                                            password=self.neo4j_password)
        self.graph: Optional[Graph] = None
        self.user_prof_comp: UserProfile = UserProfile()
        self.item_prof_comp: ItemProfile = ItemProfile()
        self.use_rnn_embedding: bool = use_rnn_embedding
        self.all_embeddings: bool = gv.all_embeddings

    def set_users(self, users: iter):
        self.users: iter = users

    def get_users(self):
        return self.users

    def compute_user_embeddings(self, users: iter):
        response: iter = iter([])
        try:
            # Execute user profiling
            self.user_prof_comp.perform_user_profiling(users=users)
            response: iter = self.user_prof_comp.response
        except Exception as e:
            gv.logger.error(e)
        return response

    def compute_item_embeddings(self, items: iter, item_key: str):
        response: iter = iter([])
        try:
            # Pass Document embedding Flag
            # Execute user profiling
            self.item_prof_comp.perform_item_profiling(items=items,
                                                       item_key=item_key,
                                                       use_rnn=self.use_rnn_embedding,
                                                       all_embeddings=self.all_embeddings)
            response: iter = self.user_prof_comp.response
        except Exception as e:
            gv.logger.error(e)
        return response

    def get_recommendations_for_user(self, user_doc: dict, item_tag: str, items: iter):
        response: iter = iter([])
        try:
            recommended_items: list = []
            gv.logger.info("Retrieving objects of type %s for user %s", item_tag, str(user_doc.get("_id")))
            if item_tag == "job":
                # return similar jobs
                for item in items:
                    if len(recommended_items) < 1:
                        recommended_items.append({"uuid": str(item.get("_id")), "type": item_tag})
            else:
                pass
            if len(recommended_items) == 0:
                gv.logger.warning("Sorry, there are no objects of type %s in the database", item_tag)
            response = iter(recommended_items)
        except Exception as e:
            gv.logger.error(e)
        return response