import os, itertools
from managers.mongodb_manager import MongoDBManager
from components.recommender_engine import HybridRecommenderEngine
from threading import Thread
from typing import Optional
from helper import global_variables as gv


class RecommenderAPI:
    def __init__(self):
        self.parent_dir: str = os.getenv("GLOBAL_PARENT_DIR")
        self.es_host = gv.es_host
        self.es_port = gv.es_port
        self.mongodb_manager = MongoDBManager(host=gv.db_host, port=gv.db_port)
        self.recommender_engine = HybridRecommenderEngine()
        self.data: Optional[dict] = None
        if gv.logger is None:
            gv.init()

    def init_mongodb_connection(self):
        try:
            if self.mongodb_manager.db_client is None:
                self.mongodb_manager.set_up_db()
        except Exception as e:
            gv.logger.error(e)

    """ 
        =======================================================================================
        ---------------------------------------- API ------------------------------------------
        =======================================================================================
    """

    def create_document(self, data: dict, index_name: str, task_name: str):
        output: dict = {"Task": task_name, "Error": "", "response": ""}
        try:
            # 1) Check ES connection
            self.init_mongodb_connection()

            # Ingest data into Mongodb
            res = self.mongodb_manager.insert_document_to_collection(document=data,
                                                                     collection_name=index_name)
            if res:
                output["response"] = "Done!"
        except Exception as e:
            gv.logger.error(e)
            output["Error"] = str(e)
        return output

    def get_document_by_id(self, doc_id: str, index_name: str, task_name: str):
        output: dict = {"Task": task_name, "Error": "",
                        "response": {}}
        try:
            # 1) Check ES connection
            self.init_mongodb_connection()
            output["response"] = self.mongodb_manager.get_document_by_id(collection_name=index_name,
                                                                         uuid=doc_id)
        except Exception as e:
            gv.logger.error(e)
            output["Error"] = str(e)
        return output

    def get_all_documents(self, index_name: str, task_name: str):
        output: dict = {"Task": task_name, "Error": "",
                        "response": iter([])}
        try:
            # 1) Check ES connection
            self.init_mongodb_connection()
            output["response"] = iter(self.mongodb_manager.get_all_documents_from_collection(
                collection_name=index_name))
        except Exception as e:
            gv.logger.error(e)
            output["Error"] = str(e)
        return output

    """ 
    Hybrid Recommender System Endpoints
    """

    @staticmethod
    def get_all_item_tags():
        output: dict = {"job": gv.jobs_index_name, "skill":gv.skills_index_name,
                        "place": gv.places_index_name, "event": gv.event_index_name,
                        "content": gv.content_index_name}
        return output

    def retrieve_user_items(self):
        response: dict = {"users": [], "items": []}
        try:
            # Get users
            task_name_users: str = "get_all_users"
            response_users: dict = self.get_all_documents(index_name=gv.users_index_name,
                                                          task_name=task_name_users)
            # Aggregate response
            response["users"] = iter(response_users["response"])

            # Get all items
            item_tags = self.get_all_item_tags()
            for item_tag, es_index in item_tags.items():
                task_name_items: str = "get_all_" + item_tag
                response_items: dict = self.get_all_documents(index_name=es_index,
                                                              task_name=task_name_items)
                # Aggregate response
                response["items"].append({item_tag: response_items["response"]})
            response["items"] = iter(response["items"])
        except Exception as e:
            gv.logger.error(e)
        return response

    def launch_hybrid_engine(self, task_name: str):
        output: dict = {"Task": task_name, "Error": "", "response": "Running"}
        try:
            # 1) RETRIEVE DATA
            self.data = self.retrieve_user_items()
            # 2) USER-ITEM PROFILING (Asynchronous Process)
            # User Profile Process
            task_name_up = "compute_user_profile"
            self.launch_batch_thread(task_name=task_name_up,
                                     target_func=self.execute_user_profiling_process)

            thread_names: list = [i.name for i in gv.batch_threads]
            thread_idx: int = thread_names.index(task_name_up)
            thread_up: Thread = gv.batch_threads[thread_idx]

            # Item Profile Process
            task_name_ip = "compute_item_profile"
            self.launch_batch_thread(task_name=task_name_ip,
                                     target_func=self.execute_item_profiling_process)

            thread_names: list = [i.name for i in gv.batch_threads]
            thread_idx: int = thread_names.index(task_name_ip)
            thread_ip: Thread = gv.batch_threads[thread_idx]

            # 3) SKILL-MATCHING (once above process have finished)
            # TODO: CALL SKILL-MATCHING TO TRAIN THE NETWORK USING USER PROFILING + ITEM PROFILING
            thread_up.join()
            thread_ip.join()
            gv.logger.info("End of Analysis")
        except Exception as e:
            gv.logger.error(e)
            output["Error"] = str(e)
        return output

    @staticmethod
    def launch_batch_thread(task_name: str, target_func: any):
        try:
            add_thread = True
            # If the list of threads is not empty
            if gv.batch_threads:
                # Check if there is a thread with the same name running
                thread_names = [i.name for i in gv.batch_threads]
                # Thread with the same name
                if task_name in thread_names:
                    thread_idx = thread_names.index(task_name)
                    # If it is still alive
                    if gv.batch_threads[thread_idx].is_alive():
                        add_thread = False
                        gv.logger.warning("A Streaming Process is already running")
                    else:
                        # remove old thread from list
                        gv.batch_threads.pop(thread_idx)
            if add_thread:
                # Create new Thread
                bath_thread: Thread = Thread(name=task_name,
                                             target=target_func)
                # Start Thread process
                bath_thread.start()
                gv.logger.warning("Thread %s has started!", str(bath_thread.name))
                gv.batch_threads.append(bath_thread)
        except Exception as e:
            gv.logger.error(e)

    def execute_user_profiling_process(self):
        try:
            """
                Batch Process
            """
            users: iter = self.data["users"]

            # Launch user profiling
            res_up: iter = self.recommender_engine.compute_user_embeddings(users=users)
            task_name = "create_user_profiling"

            # Save embeddings into DB
            for user in res_up:
                if user:
                    data: dict = {"uuid": user["uuid"], "embedding": user["embedding"]}
                    res = self.create_document(data=data,
                                               index_name=gv.user_embd_index_name,
                                               task_name=task_name)
        except Exception as e:
            gv.logger.error(e)

    def execute_item_profiling_process(self):
        try:
            items: iter = self.data["items"]
            items_names = ["job"]
            for item_key in items_names:
                # Launch item profiling
                res_ip: iter = self.recommender_engine.compute_item_embeddings(items=items,
                                                                               item_key=item_key)
                task_name: str = "create_item_profiling"

                # Save embeddings into DB
                for item in res_ip:
                    if item:
                        data: dict = {"uuid": item["uuid"], "embedding": item["embedding"],
                                      "embedding_type": item_key}
                        res = self.create_document(data=data, index_name=gv.item_embd_index_name,
                                                   task_name=task_name)
        except Exception as e:
            gv.logger.error(e)

    def get_recommendation_by_user_id(self, user_id: str, item_tag: str, task_name: str):
        output: dict = {"Task": task_name, "Error": "", "items": iter([])}
        try:
            """
                Online Process
            """
            # Get document of user
            user_response: dict = self.get_document_by_id(doc_id=user_id, index_name=gv.users_index_name,
                                                          task_name="get_user_by_id")
            user_doc = user_response["response"]
            if not user_doc:
                gv.logger.warning("There is no user in database %s with id %s", str(gv.users_index_name),
                                  str(user_id))
                output["items"] = SerializableGenerator(iter([]))
            else:
                all_items_docs: dict = self.get_all_documents(index_name=gv.jobs_index_name,
                                                              task_name="get_all_jobs")
                job_docs = all_items_docs["response"]
                items_uuids: iter = self.recommender_engine.get_recommendations_for_user(user_doc=user_doc,
                                                                                         items=job_docs,
                                                                                         item_tag=item_tag)
                output["items"] = SerializableGenerator(items_uuids)
        except Exception as e:
            gv.logger.error(e)
        return output


class SerializableGenerator(list):
    """Generator that is serializable by JSON

    It is useful for serializing huge data by JSON
    json.dumps(SerializableGenerator(iter([1, 2])))
    "[1, 2]"
    json.dumps(SerializableGenerator(iter([])))
    "[]"

    It can be used in a generator of json chunks used e.g. for a stream
    iter_json = ison.JSONEncoder().iterencode(SerializableGenerator(iter([])))
    tuple(iter_json)
    ('[1', ']')
    # >>> for chunk in iter_json:
    # ...     stream.write(chunk)
    # >>> SerializableGenerator((x for x in range(3)))
    # [<generator object <genexpr> at 0x7f858b5180f8>]
    """

    def __init__(self, iterable):
        list.__init__(self)
        tmp_body = iter(iterable)
        try:
            self._head = iter([next(tmp_body)])
            self.append(tmp_body)
        except StopIteration:
            self._head = []

    def __iter__(self):
        return itertools.chain(self._head, *self[:1])