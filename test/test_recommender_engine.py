from helper import global_variables as gv
from components.recommender_engine import HybridRecommenderEngine
import pandas as pd

gv.init()
output: dict = gv.recommender_api.retrieve_user_items()
rec_engine = HybridRecommenderEngine()


# Get data
users_data_source: list = []
for user in output["users"]:
    users_data_source.append((user["_source"]))


# Bulk data into Neo4j
rec_engine.neo4j_connector.init_graph()
rec_engine.graph = rec_engine.neo4j_connector.graph

users_df: pd.DataFrame = pd.DataFrame(users_data_source)
label_user = "user"
query = rec_engine.neo4j_connector.create_node(label=label_user.title(), params_keys=list(users_df.columns))

rec_engine.neo4j_connector.create_node_from_df(df=users_df, graph=rec_engine.graph, query=query)