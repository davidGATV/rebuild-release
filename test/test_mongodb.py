from managers.mongodb_manager import MongoDBManager
from helper import global_variables as gv
import pandas as pd


gv.init()

db_host = "localhost"
db_port = "27017"
mongb_db_man = MongoDBManager(host=db_host, port=db_port)

mongb_db_man.set_up_db()

mongb_db_man.create_db_connection()


n_users = 10

converters = {"interests": lambda x: x.strip("[]").replace("'", "").split(", "),
              "other_languages": lambda x: x.strip("[]").replace("'", "").split(", "),
              "skills_uuids": lambda x: x.strip("[]").replace("'", "").split(", "),
              "skills_level": lambda x: x.strip("[]").replace("'", "").split(", ")}

all_users: pd.DataFrame = pd.read_csv("resources//users.csv", converters=converters)
sample_user_data: pd.DataFrame = all_users.sample(n_users)
users_data: list = sample_user_data.to_dict("records")


data = users_data[0]
index_name = gv.users_index_name

res = mongb_db_man.insert_document_to_collection(document=data,
                                                 collection_name=index_name)

# Retrieve data from id

user_id = "5ebaca8a6b189973efac4be0"

dbClient = mongb_db_man.db
res_id = mongb_db_man.get_document_by_id(collection_name=index_name, uuid=user_id)

res_all = mongb_db_man.get_all_documents_from_collection(collection_name=index_name)
