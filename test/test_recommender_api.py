from api.recommender_api import RecommenderAPI

api = RecommenderAPI()

data = api.retrieve_user_items()

users = data["users"]

for user in users:
    print(user)

response_train = api.launch_hybrid_engine(task_name="testing_train")

user_id = "5ebac9f139bb6d1823ac7038"
response_test = api.get_recommendation_by_user_id(user_id=user_id, item_tag="job",
                                                  task_name="get_recommendation")

recommended_items = []

for item in users:
    recommended_items.append({"uuid": str(item.get("_id")), "type": "user"})
