from dotmap import DotMap
from helper.rebuild_utils import generate_uuid_from_string
from helper import global_variables as gv


class JobField:
    def __init__(self, name: {str}, description: {str}, uuid=None):
        self.name = name
        self.description = description
        if self.uuid is None:
            self.uuid = self.generate_uuid([self.name])
        else:
            self.uuid = uuid

    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)

    @staticmethod
    def generate_uuid(data_uuid: {list}):
        return generate_uuid_from_string(data_uuid)


class GeoLocalization:
    def __init__(self, localization: {str}, uuid=None):
        self.localization = self.convert_localization(localization)
        self.address = self.localization.address
        self.altitude = self.localization.altitude
        self.latitude = self.localization.latitude
        self.longitude = self.localization.longitude
        self.place_id = self.localization.raw["place_id"]
        if self.uuid is None:
            self.uuid = self.generate_uuid([self.place_id])
        else:
            self.uuid = uuid

    @staticmethod
    def generate_uuid(data_uuid: {list}):
        return generate_uuid_from_string(data_uuid)

    @staticmethod
    def convert_localization(localization_str: {str}):
        location = None
        try:
            location = gv.locator.geocode(localization_str)
        except Exception as e:
            gv.logger.error(e)
        return location


class Job:
    def __init__(self, title: {str}, api_uuid: {str}, normalized_job_title: {str},
                 description: {str}, skills_uuids: {list},
                 skill_importances: {list}, localization=None, uuid=None):

        self.title = title
        self.api_uuid = api_uuid
        self.normalized_job_title = normalized_job_title
        self.description = description
        self.skills_uuids = [self.generate_uuid([sk_uuid]) for sk_uuid in skills_uuids]
        self.skills_importances = skill_importances
        if localization is None:
            self.localization = "Not Available"

        if uuid is None:
            self.uuid = self.generate_uuid([self.api_uuid])
        else:
            self.uuid = uuid

    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)

    @staticmethod
    def generate_uuid(data_uuid: {list}):
        return generate_uuid_from_string(data_uuid)