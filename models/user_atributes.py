from datetime import datetime
from models.rebuild_objects import RebuildObject


class EducationalExperience(RebuildObject):
    def __init__(self, country: {str}, started_at : {datetime}, ended_at: {datetime},
                 institute: {str}, subject: {str}, type: {int}, id=None):
        self.country = country
        self.started_at = started_at
        self.ended_at = ended_at
        self.institute = institute
        self.subject = subject
        self.type = type

        if id is None:
            self.id = self.generate_uuid([self.institute, self.type])
        else:
            self.id = id


class WorkExperience(RebuildObject):
    def __init__(self, category: {str}, company: {str}, started_at : {datetime},
                 ended_at: {datetime}, job_task: {str}, id=None):

        self.category = category
        self.company = company
        self.started_at = started_at
        self.ended_at = ended_at
        self.job_task = job_task
        if id is None:
            self.id = self.generate_uuid([self.job_task, self.company])
        else:
            self.id = id


class LanguageAttribute(RebuildObject):
    def __init__(self, language: {str}, reading_level: {int}, spoken_level: {int},
                 written_level: {int}, id=None):
        self.language = language
        self.reading_level = reading_level
        self.spoken_level = spoken_level
        self.written_level = written_level
        if id is None:
            self.id = self.generate_uuid([self.language])
        else:
            self.id = id


class Hobbies(RebuildObject):
    def __init__(self, name: {str}, description: {str}, id=None):
        self.name = name
        self.description = description
        if id is None:
            self.id = self.generate_uuid([self.name, self.description])
        else:
            self.id = id
