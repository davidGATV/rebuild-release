from dotmap import DotMap
from helper.rebuild_utils import generate_uuid_from_string


class SkillType:
    def __init__(self, name: {str}, description: {str}, uuid=None):
        self.name = name
        self.description = description
        if self.uuid is None:
            self.uuid = self.generate_uuid([self.name, self.description])
        else:
            self.uuid = uuid

    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)

    @staticmethod
    def generate_uuid(data_uuid: {list}):
        return generate_uuid_from_string(data_uuid)


class Skill:
    def __init__(self, name: {str}, api_uuid: {str}, description: {str}, level: {float},
                 skill_type: {str}, normalized_skill_name: {str}, uuid=None):

        self.name = name
        self.api_uuid = api_uuid
        self.description = description
        self.level = level
        self.skill_type = skill_type
        self.normalized_skill_name = normalized_skill_name

        if uuid is None:
            self.uuid = self.generate_uuid([self.api_uuid])
        else:
            self.uuid = uuid

    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)

    @staticmethod
    def generate_uuid(data_uuid: {list}):
        return generate_uuid_from_string(data_uuid)
