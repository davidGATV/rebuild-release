from dotmap import DotMap
from helper.rebuild_utils import generate_uuid_from_string


class RebuildObject:
    def dict_from_class(self):
        return dict((key, value)
                    for (key, value) in self.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)

    @staticmethod
    def generate_uuid(data_uuid: {list}):
        return generate_uuid_from_string(data_uuid)