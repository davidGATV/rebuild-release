from dotmap import DotMap
from helper.rebuild_utils import generate_uuid_from_string


class User:
    def __init__(self, username: str, email: str, age: str,
                 sex: str, mother_tongue: str, studies_level: str,
                 skills: list, interests: list, origin_country: str= None,
                 other_languages: list = None, arrival_country: str = None, reason: str = None, uuid: str = None):

        self.username: str = username
        self.email: str = email
        self.age: str = age
        self.sex: str = sex
        self.mother_tongue: str = mother_tongue
        self.studies_level: str = studies_level
        self.skills: list = skills
        self.interests: list = interests
        self.origin_country: str = origin_country
        self.other_languages: list = other_languages
        self.arrival_country: str = arrival_country
        self.reason: str = reason
        if uuid is None:
            self.uuid: str = self.generate_uuid([self.email])
        else:
            self.uuid: str = uuid
    
    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: dict):
        return DotMap(data_dict)

    @staticmethod
    def generate_uuid(data_uuid: list):
        return generate_uuid_from_string(data_uuid)
    
    @staticmethod
    def get_properties():
        return {"username": "str", "email": "str",
                "age": "str", "sex": "str", "mother_tongue": "str",
                "studies_level": "str", "skills": "list", "interests": "list",
                "origin_country": "str", "other_languages": "list", "arrival_country": "str",
                "reason": "str", "uuid": "str"}