from dotmap import DotMap
from helper.rebuild_utils import generate_uuid_from_string


class InterestType:
    def __init__(self, name: {str}, description: {str}, uuid=None):
        self.name = name
        self.description = description
        if self.uuid is None:
            self.uuid = self.generate_uuid([self.name, self.description])
        else:
            self.uuid = uuid

    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)

    @staticmethod
    def generate_uuid(data_uuid: {list}):
        return generate_uuid_from_string(data_uuid)


class Interest:
    def __init__(self, name: {str}, description: {str}, type: {InterestType}, uuid=None):

        self.name = name
        self.description = description
        self.type = type
        if self.uuid is None:
            self.uuid = self.generate_uuid([self.name, self.description])
        else:
            self.uuid = uuid

    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)

    @staticmethod
    def generate_uuid(data_uuid: {list}):
        return generate_uuid_from_string(data_uuid)