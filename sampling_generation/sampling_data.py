import numpy as np
import random
from helper import global_variables as gv
from helper.rebuild_utils import get_location, get_country_info, get_n_jobs, get_skills_for_job
from faker import Faker


class SamplingUser:
    def __init__(self, skills_data: {list}):
        if gv.logger is None:
            gv.init()

        self.faker = Faker()
        self.sex = np.random.choice(["F", "M"])
        self.profile = self.get_user_profile(faker=self.faker, sex=self.sex)
        self.username = self.profile["username"]
        self.email = self.profile["mail"]
        self.age = self.get_age_range()

        self.origin_city, self.origin_country_code = self.get_country_city_from_location(self.faker)
        self.origin_country = self.get_origin_country_from_code(self.origin_country_code)
        self.mother_tongue = self.get_mother_tongue(self.origin_country_code)

        self.studies_level = self.get_studies_level()
        self.n_skills = np.random.choice([i for i in range(1, 10)])
        self.skills = self.get_skills(skills_data=skills_data,
                                      n_skills=self.n_skills)

        self.n_interests = np.random.choice([1, 2, 3])
        self.interests = self.get_interests(n=self.n_interests)

        self.n_lang = np.random.choice([1, 2, 3])
        self.other_languages = self.get_other_languages(self.origin_country_code, n=self.n_lang)

        self.arrival_country = self.get_arrival_country()
        self.reason = self.get_reason()

    @staticmethod
    def get_user_profile(faker: {Faker}, sex: {str}):
        return faker.profile(sex=sex)

    @staticmethod
    def get_age_range():
        age = random.choice([i for i in range(18, 55)])
        if age <= 18:
            age = 19
        age_range = "0-10"

        if 10 <= age <= 18:
            age_range = "11-18"
        elif 19 <= age <= 35:
            age_range = "19-35"
        elif 36 <= age <= 55:
            age_range = "36-55"
        elif 56 <= age <= 70:
            age_range = "56-70"
        elif age > 70:
            age_range = ">70"
        return age_range

    @staticmethod
    def get_reason():
        return np.random.choice(["war", "asylum", "family", "other"])

    @staticmethod
    def get_country_city_from_location(faker: {Faker}):
        return get_location(faker)

    @staticmethod
    def get_arrival_country():
        country_code = np.random.choice(["ES", "FR", "IT", "BE", "GR"])
        response = get_country_info(country_code)
        return response.name

    @staticmethod
    def get_origin_country_from_code(country_code: {str}):
        response = get_country_info(country_code)
        return response.name

    @staticmethod
    def get_mother_tongue(country_code: {str}):
        response = get_country_info(country_code)
        language = response.languages[0]["name"]
        return language.title()

    @staticmethod
    def get_other_languages(country_code: {str}, n=2):
        mother_tongue = __class__.get_mother_tongue(country_code)
        all_languages = ["French", "Spanish", "Italian", "Greek",
                         "English", "Arabic", "Chinese", "Japanese"]
        if mother_tongue in all_languages:
            all_languages.remove(mother_tongue)
        return random.sample(all_languages, n)

    @staticmethod
    def get_studies_level():
        return np.random.choice(["primary", "high-school",
                                 "university", "doctorate"])

    @staticmethod
    def get_interests(n=2):
        all_interests = ["Music", "Sports", "Culture", "History", "languages",
                         "Science"]
        return random.sample(all_interests, n)

    @staticmethod
    def get_skills(skills_data: {list}, n_skills=2):
        skills = SamplingSkills(skills_data=skills_data,
                                n_skills=n_skills)
        return skills.output_skills

    def get_user_dict(self):
        response = {"username": self.username, "email": self.email, "age": self.age,
                    "sex": self.sex, "mother_tongue": self.mother_tongue,
                    "studies_level": self.studies_level, "skills": self.skills,
                    "interests": self.interests, "origin_country": self.origin_country,
                    "other_languages": self.other_languages, "arrival_country": self.arrival_country,
                    "reason": self.reason}
        return response


class SamplingSkills:
    def __init__(self, skills_data: {list}, n_skills=2):
        if gv.logger is None:
            gv.init()
        self.skills_data = skills_data
        self.skills = self.get_skills(self.skills_data, n_skills=n_skills)
        self.output_skills = self.get_raw_data(skills=self.skills)

    @staticmethod
    def get_skills(skill_data: {list}, n_skills=3):
        return random.sample(skill_data, n_skills)

    @staticmethod
    def get_skills_levels():
        return random.choice([1, 2, 3, 4, 5])

    @staticmethod
    def get_raw_data(skills: {list}):
        output_skills = []
        for i, s in enumerate(skills):
            s.pop('importance', None)
            s.pop('level', None)
            s["level"] = __class__.get_skills_levels()
            output_skills.append(s)
        return output_skills


class SamplingJobsSkills:
    def __init__(self, n_jobs=1500):
        if gv.logger is None:
            gv.init()
        self.n_jobs = n_jobs
        self.jobs = self.get_job_data(n_jobs=self.n_jobs)
        self.uuids = [i["uuid"] for i in self.jobs]
        self.job_skills = self.get_job_skills(job_uuids=self.uuids)

    @staticmethod
    def retrieve_skills_jobs(job_skills: {list}):
        all_skills = []
        all_jobs = []
        try:
            for i in job_skills:
                if "skills" in list(i.keys()):
                    all_skills += [j for j in i["skills"]]
                    job_data = {"job_uuid": i["job_uuid"],
                                "job_title": i["job_title"],
                                "normalized_job_title": i["normalized_job_title"],
                                "description": "",
                                "skill_names": [j["skill_name"] for j in i["skills"]],
                                "skills_uuids": [j["skill_uuid"] for j in i["skills"]],
                                "skill_importances": [j["importance"] for j in i["skills"]]}
                    all_jobs.append(job_data)
        except Exception as e:
            gv.logger.error(e)
        return all_jobs, all_skills

    @staticmethod
    def get_job_skills(job_uuids):
        jobs_skills = []
        try:
            for job_uuid in job_uuids:
                jobs_skills.append(get_skills_for_job(job_uuid))
        except Exception as e:
            gv.logger.error(e)
        return jobs_skills

    @staticmethod
    def get_job_data(n_jobs):
        return get_n_jobs(n_jobs=n_jobs)

    @staticmethod
    def get_job_names(job_data):
        return [i["title"] for i in job_data]

    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())