import numpy as np
import random
from helper import global_variables as gv
from faker import Faker
from datetime import date
from models.skill import Skill
from models.user_atributes import (Hobbies, EducationalExperience,
                                           LanguageAttribute, WorkExperience)
from helper.rebuild_utils import (get_location, get_country_info, get_n_jobs,
                                  get_skills_for_job, calculate_age_from_birth_year,
                                  generate_school_experience, generate_university_experience,
                                  random_date, generate_work_experience)
from models.object_dict import ObjectDict

# ====================================================================
# --------------------------
# =====================================================================


class SamplingJobsSkills:
    def __init__(self, n_jobs=1500):
        if gv.logger is None:
            gv.init()
        self.n_jobs = n_jobs
        self.jobs = self.get_job_data(n_jobs=self.n_jobs)
        self.uuids = [i["uuid"] for i in self.jobs]
        self.job_skills = self.get_job_skills(job_uuids=self.uuids)

    @staticmethod
    def retrieve_skills_jobs(job_skills: {list}):
        all_skills = []
        all_jobs = []
        try:
            for i in job_skills:
                if "skills" in list(i.keys()):
                    all_skills += [j for j in i["skills"]]
                    job_data = {"job_uuid": i["job_uuid"],
                                "job_title": i["job_title"],
                                "normalized_job_title": i["normalized_job_title"],
                                "description": "",
                                "skill_names": [j["skill_name"] for j in i["skills"]],
                                "skills_uuids": [j["skill_uuid"] for j in i["skills"]],
                                "skill_importances": [j["importance"] for j in i["skills"]]}
                    all_jobs.append(job_data)
        except Exception as e:
            gv.logger.error(e)
        return all_jobs, all_skills

    @staticmethod
    def get_job_skills(job_uuids):
        jobs_skills = []
        try:
            for job_uuid in job_uuids:
                jobs_skills.append(get_skills_for_job(job_uuid))
        except Exception as e:
            gv.logger.error(e)
        return jobs_skills

    @staticmethod
    def get_job_data(n_jobs):
        return get_n_jobs(n_jobs=n_jobs)

    @staticmethod
    def get_job_names(job_data):
        return [i["title"] for i in job_data]

    @staticmethod
    def dict_from_class(cls):
        return dict((key, value)
                    for (key, value) in cls.__dict__.items())


class SamplingUser:
    def __init__(self, jobs: {list}):
        if gv.logger is None:
            gv.init()
        self.total_jobs = jobs
        self.provider = self.retrieve_localized_providers()
        self.faker = Faker(self.provider)

        self.mobile_phone = self.faker.phone_number()
        self.fk_place_birth, self.fiscal_code = self.generate_location(self.faker)
        self.country_response = self.country_information(self.fiscal_code)
        self.gender = np.random.choice(["F", "M"])
        self.profile = self.get_user_profile(faker=self.faker, sex=self.gender)
        # Avoid Parsing Arabic names
        if "ar" or "hy" in self.provider:
            self.faker_eng = Faker("en_GB")
            self.profile["name"] = self.faker_eng.name()

        self.full_name = self.profile["name"]
        self.alternative_name = self.profile["username"]
        self.birth_name = " ".join(self.profile["name"].split(" ")[:-1])
        self.family_name = self.profile["name"].split(" ")[-1]
        self.email_address = self.profile["mail"]
        self.date_of_birth = self.generate_random_birthdate()
        self.age = self.get_age_from_year(self.date_of_birth.year)
        self.marital_status = self.generate_marital_status(age=self.age)
        self.arrival_date = self.generate_arrival_date(self.faker)
        self.duration_since_arrival = (date.today() - self.arrival_date).days
        self.mother_tongue = self.generate_mother_tongue(self.country_response)
        self.profile_summary = ""
        self.resident_status = self.generate_resident_status()
        self.work_permit = self.generate_work_permission()
        self.fk_citizenship, self.fk_country_birth = self.retrieve_country_data(self.country_response)
        self.fk_residency = self.generate_arrival_country()
        self.fk_residency_location = ""
        self.educational_level = self.generate_educational_level()
        self.educational_experience = self.generate_educational_experience(country=self.fk_country_birth,
                                                                           age=self.age,
                                                                           educational_level=self.educational_level)
        self.hobbies, self.hobbies_ids, self.hobbies_names = self.generate_hobbies()
        self.languages, self.languages_names = self.generate_languages(res=self.country_response,
                                                 educational_level=self.educational_level)
        self.driving_license = self.get_driving_license(self.age)
        self.work_experience, self.work_experience_name, self.skills_work_experience, self.additional_skills = self.generate_work_skills_experience(age=self.age,
                                                                                 profile=self.profile,
                                                                                 ended_date=self.arrival_date,
                                                                                 jobs=jobs)
        self.skills_work_names, self.skills_work_uuids, self.skills_work_level = self.get_skills_work_data(self.skills_work_experience)
        self.additional_skills_names, self.additional_skills_uuids, self.additional_skills_level = self.get_additional_skills_data(self.additional_skills)

    @staticmethod
    def retrieve_localized_providers():
        local_prov = ["ar_EG", "ar_PS", "ar_SA",        # Arabic (Egypt), Arabic (Palestine), Arabic (Saudi Arabia)
                      "es_ES", "fa_IR", "it_IT",        # Spanish (Spain), Persian (Iran), Italian
                      "ka_GE", "pt_BR", "hy_AM"]        # Georgian (Georgia), Portuguese (Brazil), Armenian
        return np.random.choice(local_prov)

    @staticmethod
    def get_user_profile(faker: {Faker}, sex: {str}):
        return faker.profile(sex=sex)

    @staticmethod
    def get_age_from_year(birth_year):
        return calculate_age_from_birth_year(birth_year)

    @staticmethod
    def generate_random_birthdate():
        return random_date(start_date=date(1945, 2, 17),
                           end_date=date(2000, 2, 17))

    @staticmethod
    def country_information(country_code: {str}):
        return get_country_info(country_code)

    @staticmethod
    def generate_mother_tongue(response: {ObjectDict}):
        language = response.languages[0]["name"]
        return language.title()

    @staticmethod
    def generate_arrival_date(fake: {Faker}):
        return fake.date_between(start_date='-5y', end_date='today')

    @staticmethod
    def generate_fiscal_code(faker: {Faker}):
        return __class__.generate_location(faker)[1]

    @staticmethod
    def generate_location(faker: {Faker}):
        return get_location(faker)

    @staticmethod
    def generate_marital_status(age: {int}):
        if age <= 20:
            statuses = {"single": 1}
        elif 20 < age <= 30:
            statuses = {"single": 1, "married": 2, "other": 5}
        else:
            statuses = {"single": 1, "married": 2, "divorced": 3,
                        "widowed": 4, "other": 5}
        return np.random.choice(list(statuses.values()))

    @staticmethod
    def generate_resident_status():
        statuses = {"permanent": 1, "conditional": 2,
                    "temporal": 3}
        return np.random.choice(list(statuses.values()))

    @staticmethod
    def generate_work_permission():
        statuses = {"permanent": 1, "conditional": 2,
                    "temporal": 3}
        return np.random.choice(list(statuses.values()))

    @staticmethod
    def retrieve_country_data(response: {ObjectDict}):
        return response.demonym, response.name

    @staticmethod
    def generate_arrival_country():
        arrival_country = np.random.choice(["Spain", "France", "Italy", "Belgium", "Greece"])
        return arrival_country

    @staticmethod
    def generate_hobbies():
        n = np.random.choice([i for i in range(1, 5)])
        all_hobbies = ["reading", "listening to music", "sports", "watching tv",
                       "writing", "playing instruments", "photography",
                       "travelling", "physical activity"]

        names = random.sample(all_hobbies, n)
        hobbies = [Hobbies(name=names[i], description="") for i in range(len(names))]
        hobbies_ids = [all_hobbies.index(i) for i in names]
        hobbies_names = names
        return hobbies, hobbies_ids, hobbies_names

    @staticmethod
    def get_language_levels(educational_level: {str}):
        # no studies
        if educational_level == "no education":
            reading_level = 2
            written_level = 2
            spoken_level = 4
        elif educational_level == "primary":
            reading_level = 3
            written_level = 3
            spoken_level = 4
        elif educational_level == "secondary":
            reading_level = 4
            written_level = 4
            spoken_level = 4
        # University
        else:
            reading_level = 5
            written_level = 5
            spoken_level = 5
        return reading_level, written_level, spoken_level

    @staticmethod
    def generate_languages(res: {ObjectDict}, educational_level: {str}):
        response = res.languages
        original_language_names = [i["name"] for i in response]
        # no studies
        reading_level, written_level, spoken_level = __class__.get_language_levels(educational_level)

        languages = [LanguageAttribute(language=i["name"],
                                       reading_level=reading_level,
                                       written_level=written_level,
                                       spoken_level=spoken_level) for i in response]
        # Additional languages
        n = np.random.choice([i for i in range(1, 3)])
        alternative_languages = ["French", "Spanish", "Italian", "Greek",
                                 "English", "Arabic", "German", "Farsi", "Turkish"]
        alternative_languages += original_language_names

        # Remove duplicates
        alternative_languages = list(set(alternative_languages))
        other_langs = random.sample(alternative_languages, n)
        languages += [LanguageAttribute(language=i,
                                        reading_level=reading_level-1,
                                        written_level=written_level-1,
                                        spoken_level=spoken_level-2) for i in other_langs if i not in original_language_names]
        languages_names = [i.language for i in languages]
        return languages, languages_names

    @staticmethod
    def get_educational_levels():
        return {"no education": 1, "primary": 2, "secondary": 3, "university":4}

    @staticmethod
    def generate_educational_level():
        return np.random.choice(list(__class__.get_educational_levels().keys()))

    @staticmethod
    def generate_educational_experience(country: {str}, age: {int}, educational_level: {str}):
        educational_experience = []
        # country: {str}, started_at : {datetime}, ended_at: {datetime},
        #                  institute: {str}, subject: {str}, type:{int}, id=None

        if educational_level != "no education":
            response = generate_school_experience(country=country, age=age)
            educational_experience.append(EducationalExperience(country=country,
                                                                started_at=response["started_at"],
                                                                ended_at=response["ended_at"],
                                                                institute=response["institute"],
                                                                subject="", type="primary"))
        if educational_level == "secondary":
            response = generate_school_experience(country=country, age=age, high_school=True)
            educational_experience.append(EducationalExperience(country=country,
                                                                started_at=response["started_at"],
                                                                ended_at=response["ended_at"],
                                                                institute=response["institute"],
                                                                subject="", type="secondary"))
        elif educational_level == "university":
            # High-school
            response = generate_school_experience(country=country, age=age, high_school=True)
            educational_experience.append(EducationalExperience(country=country,
                                                                started_at=response["started_at"],
                                                                ended_at=response["ended_at"],
                                                                institute=response["institute"],
                                                                subject="", type="secondary"))
            # University
            response = generate_university_experience(country=country, age=age)
            educational_experience.append(EducationalExperience(country=country,
                                                                started_at=response["started_at"],
                                                                ended_at=response["ended_at"],
                                                                institute=response["institute"],
                                                                subject="", type="university"))
        return educational_experience

    @staticmethod
    def generate_work_skills_experience(age: {int}, profile: {dict}, ended_date: {date}, jobs: {list}):
        work_experience, skills_experience, additional_skills = [], [], []
        min_age = 22
        if age >= min_age:
            response = generate_work_experience(age=age, profile=profile,
                                                ended_date=ended_date,
                                                min_age=(min_age+1))
            job = np.random.choice(jobs)
            while "error" in job:
                job = np.random.choice(jobs)

            work_experience.append(WorkExperience(category=job["job_title"],
                                                  company=response["company"],
                                                  started_at=response["started_at"],
                                                  ended_at=response["ended_at"],
                                                  job_task=response["job_task"]))

            n = np.random.choice([4, 5, 6, 7, 8])           # the number of skills for the user
            for i in range(0, n):
                skill = {}
                pos = np.random.choice(np.arange(0, 30))
                skill["skill_name"] = job["skill_names"][pos]
                skill["skill_uuid"] = job["skills_uuids"][pos]
                skills_experience.append(skill)
            n = np.random.choice([2, 3, 4, 5])
            for i in range(0, n):
                skill = {}
                pos = np.random.choice(np.arange(31, 119))
                skill["skill_name"] = job["skill_names"][pos]
                skill["skill_uuid"] = job["skills_uuids"][pos]
                additional_skills.append(skill)

            work_experience_name = work_experience[0].category
        else:
            work_experience_name = " "
            n = np.random.choice([4, 5, 6, 7, 8])
            for i in range(0, n):
                skill = {}
                pos = np.random.choice(np.arange(0, 119))
                skill["skill_name"] = jobs[0]["skill_names"][pos]
                skill["skill_uuid"] = jobs[0]["skills_uuids"][pos]
                additional_skills.append(skill)

        return work_experience, work_experience_name, skills_experience, additional_skills


    @staticmethod
    def get_skills_work_data(skills: {list}):
        skills_work_names = [i["skill_name"] for i in skills]
        skills_work_uuids = [i["skill_uuid"] for i in skills]
        skills_work_level = [np.random.choice([1, 2, 3, 4, 5]) for i in range(0, len(skills))]
        return skills_work_names, skills_work_uuids, skills_work_level

    @staticmethod
    def get_additional_skills_data(skills: {list}):
        additional_skills_names = [i["skill_name"] for i in skills]
        additional_skills_uuids = [i["skill_uuid"] for i in skills]
        additional_skills_level = [np.random.choice([1, 2, 3, 4, 5]) for i in range(0, len(skills))]
        return additional_skills_names, additional_skills_uuids, additional_skills_level

    @staticmethod
    def get_driving_license(age: {int}):
        choises = {"no_license": 0, "A": 1, "B": 2}
        if age > 18:
            choise = np.random.choice(list(choises.values()))
        else:
            choise = 0
        return choise


    def get_user_dict(self):
        response = {"alternative_name": self.alternative_name, "email_address": self.email_address,
                    "mobile_phone": self.mobile_phone, "birth_name": self.birth_name,
                    "family_name": self.family_name, "full_name": self.full_name,
                    "gender": self.gender, "age": self.age,
                    "arrival_date": self.arrival_date, "duration_since_arrival": self.duration_since_arrival,
                    "fiscal_code": self.fiscal_code, "marital_status": self.marital_status,
                    "mother_tongue": self.mother_tongue, "profile_summary": self.profile_summary,
                    "resident_status": self.resident_status, "work_permit": self.work_permit,
                    "fk_citizenship": self.fk_citizenship, "fk_country_birth": self.fk_country_birth,
                    "fk_place_birth": self.fk_place_birth, "fk_residency": self.fk_residency,
                    "fk_residency_location": self.fk_residency_location,
                    "driving_license": self.driving_license,
                    "skills_work_names": self.skills_work_names, "skills_work_level": self.skills_work_level, "skills_work_uuids": self.skills_work_uuids,
                    "additional_skills_names": self.additional_skills_names, "additional_skills_level": self.additional_skills_level, "additional_skills_uuids": self.additional_skills_uuids,
                    "educational_level": self.educational_level,
                    "educational_experience": self.educational_experience, "hobbies": self.hobbies,
                    "hobbies_ids": self.hobbies_ids, "hobbies_names": self.hobbies_names,
                    "work_experience": self.work_experience, "work_experience_name": self.work_experience_name,
                    "languages": self.languages, "languages_names": self.languages_names}
        return response
        # It does not save 'date_of_birth', 'additional_skills', 'skills_work_experience'


class SamplingSkills:
    def __init__(self, skills_data: {list}, n_skills=2):
        if gv.logger is None:
            gv.init()
        self.skills_data = skills_data
        self.skills = self.get_skills(self.skills_data, n_skills=n_skills)
        self.output_skills = self.get_raw_data(skills=self.skills)

    @staticmethod
    def get_skills(skill_data: {list}, n_skills=3):
        return random.sample(skill_data, n_skills)

    @staticmethod
    def get_skills_levels():
        return random.choice([1, 2, 3, 4, 5])

    @staticmethod
    def get_raw_data(skills: {list}):
        output_skills = []
        for i, s in enumerate(skills):
            s.pop('importance', None)
            s.pop('level', None)
            s["level"] = __class__.get_skills_levels()
            output_skills.append(s)
        return output_skills