import os
import csv
import pandas as pd
from helper import global_variables as gv
from data_generation.sampling_data import SamplingUser, SamplingJobsSkills
from models.user import User
from models.skill import Skill
from models.job import Job
from models.object_dict import ObjectDict


class DataGenerator:
    def __init__(self, n_jobs, n_users, data_dir: {str}):
        self.n_jobs = n_jobs
        self.n_users = n_users
        self.sampling_jobs_skills = None
        self.job_skills = None
        self.data_dir = data_dir
        if gv.logger is None:
            gv.init()

    def init_sampling_jobs_skills(self):
        self.sampling_jobs_skills = SamplingJobsSkills(self.n_jobs)
        self.job_skills = self.sampling_jobs_skills.job_skills

    def generate_job_skills_data(self):
        all_jobs, all_skills = [], []
        try:
            # Jobs and Skills
            if self.sampling_jobs_skills is None:
                self.init_sampling_jobs_skills()
            all_jobs, all_skills = self.sampling_jobs_skills.retrieve_skills_jobs(
                job_skills=self.job_skills)
        except Exception as e:
            gv.logger.error(e)
        return all_jobs, all_skills

    def generate_skills_dataframe(self, all_skills: {list}, save_data=True, filepath=None):
        skills_df = None
        try:
            all_skills_model = []
            for skill in all_skills:
                skill_map = ObjectDict(skill)
                skill_rebuild = Skill(name=skill_map.skill_name,
                                      api_uuid=skill_map.skill_uuid,
                                      description=skill_map.description,
                                      level=2.0,
                                      skill_type=skill_map.skill_type,
                                      normalized_skill_name=skill_map.normalized_skill_name)
                # Create dictionary
                data_skill = Skill.dict_from_class(skill_rebuild)
                all_skills_model.append(data_skill)

            # Skills DataFrame
            skills_df = pd.DataFrame(all_skills_model)

            # Delete Duplicates
            skills_df.drop_duplicates(subset="api_uuid",
                                      keep="first", inplace=True)

            # Drop columns
            drop_cols = ["level"]
            skills_df.drop(drop_cols, axis=1, inplace=True)

            if save_data:
                skills_path = os.path.join(self.data_dir, filepath)
                skills_df.to_csv(skills_path, index=False, sep=',', quoting=csv.QUOTE_ALL)
        except Exception as e:
            gv.logger.error(e)
        return skills_df

    def generate_jobs_dataframe(self, all_jobs: {list}, save_data=True, filepath=None):
        jobs_df = None
        try:
            all_jobs_model = []
            for job in all_jobs:
                # =======================================================================
                #                           Job Object
                # =======================================================================
                job_map = ObjectDict(job)
                job_rebuild = Job(title=job_map.job_title,
                                  api_uuid=job_map.job_uuid,
                                  normalized_job_title=job_map.normalized_job_title,
                                  description=job_map.description,
                                  skills_uuids=job_map.skills_uuids,
                                  skill_importances=job_map.skill_importances)

                # Create dictionary
                data_job = Job.dict_from_class(job_rebuild)
                all_jobs_model.append(data_job)

            jobs_df = pd.DataFrame(all_jobs_model)
            if save_data:
                jobs_path = os.path.join(self.data_dir, filepath)
                jobs_df.to_csv(jobs_path, index=False, sep=',', quoting=csv.QUOTE_ALL)
        except Exception as e:
            gv.logger.error(e)
        return jobs_df

    def generate_users_dataframe(self, skills_df: {pd.DataFrame}, save_data=True, filepath=None):
        users_df = None
        try:
            skills_data = skills_df.to_dict("records")
            all_users = []

            gv.logger.info("Generating %s users", str(self.n_users))
            for i in range(self.n_users):
                user = SamplingUser(skills_data).get_user_dict()

                user_map = ObjectDict(user)
                user_rebuild = User(username=user_map.username, email=user_map.email,
                                    age=user_map.age, sex=user_map.sex, mother_tongue=user_map.mother_tongue,
                                    studies_level=user_map.studies_level, skills=user_map.skills,
                                    interests=user_map.interests, origin_country=user_map.origin_country,
                                    other_languages=user_map.other_languages, arrival_country=user_map.arrival_country,
                                    reason=user_map.reason)

                # Create dictionary
                data_user = User.dict_from_class(user_rebuild)
                data_user["skills_uuids"] = [i["uuid"] for i in data_user["skills"]]
                data_user["skills_level"] = [i["level"] for i in data_user["skills"]]
                data_user.pop("skills", None)
                all_users.append(data_user)

            users_df = pd.DataFrame(all_users)
            if save_data:
                users_path = os.path.join(self.data_dir, filepath)
                users_df.to_csv(users_path, index=False, sep=',', quoting=csv.QUOTE_ALL)

        except Exception as e:
            gv.logger.error(e)
        return users_df