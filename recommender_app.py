import os, multiprocessing, json
from helper import global_variables as gv
from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
from api.recommender_api import RecommenderAPI


# Parameters
os.environ["GLOBAL_PARENT_DIR"] = ""
SWAGGER_URL = '/docs'
API_URL = '/static/swagger.json'
APP_NAME = "Recommender System"
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(SWAGGER_URL,
                                              API_URL,
                                              config={'app_name': APP_NAME})

# ================================================================
app = Flask(__name__,
            static_folder=os.path.join('www', 'static'),
            template_folder=os.path.join('www', "templates")
            )
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
# ================================================================


""" 
    =======================================================================================
    ----------------------------- Hybrid Recommendation Engine ----------------------------
    =======================================================================================
"""
recommender_api = RecommenderAPI()


@app.route('/service/recommendation', methods=['POST'])
def launch_recommender_engine():
    response: dict = {}
    try:
        task_name: str = "launch_hybrid_engine"
        response: dict = recommender_api.launch_hybrid_engine(task_name=task_name)
    except Exception as e:
        gv.logger.error(e)
    return json.dumps(response)


@app.route('/service/recommendation/<user_id>', methods=['GET'])
def get_recommendation_by_user_id(user_id: str):
    response: dict = {}
    try:
        task_name: str = "get_recommendations_by_user_id"
        item_tag = "job"
        response: dict = recommender_api.get_recommendation_by_user_id(user_id=user_id,
                                                                       item_tag=item_tag,
                                                                       task_name=task_name)
    except Exception as e:
        gv.logger.error(e)
    return json.dumps(response)


if __name__ == '__main__':
    multiprocessing.freeze_support()
    try:
        gv.init()
        app.run(host=gv.host, port=gv.port, debug=False, passthrough_errors=False, threaded=True)
    except Exception as e:
        print(e)